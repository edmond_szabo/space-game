﻿using System;
using System.Windows;
using System.Windows.Threading;
using System.ComponentModel;
using System.Media;
using TowerDefense.Model;
using TowerDefense.ViewModel;
using TowerDefense.View;

namespace TowerDefense
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Fields
        private GameModel _model;
        private GameViewModel _viewModel;
        private MainWindow _view;
        private DispatcherTimer _timer; // Advances game
        private DispatcherTimer _gameTime; // Measures gametime
        private SoundPlayer _player;
        private Boolean _playerStopped;
        #endregion

        #region Constructors
        /// <summary>
        /// App constructor, connects App_startup to Startup event
        /// </summary>
        public App()
        {
            Startup += new StartupEventHandler(App_Startup);
        }
        #endregion

        #region Application event handlers
        /// <summary>
        /// Initializes fields and connects events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void App_Startup(object sender, StartupEventArgs e)
        {
            _model = new GameModel();
            _model.GameCreated += new EventHandler<GameEventArgs>(Model_GameCreated);
            _model.GameOver += new EventHandler<GameEventArgs>(Model_GameOver);
            _model.NewGame(GameDifficulty.Easy);

            _viewModel = new GameViewModel(_model);
            _viewModel.NewGame += new EventHandler(ViewModel_NewGame);
            _viewModel.ExitGame += new EventHandler(ViewModel_ExitGame);
            _viewModel.PauseGame += new EventHandler(ViewModel_PauseGame);
            _viewModel.PauseMusic += new EventHandler(ViewModel_PauseMusic);

            _view = new MainWindow();
            _view.DataContext = _viewModel;
            _view.Closing += new System.ComponentModel.CancelEventHandler(View_Closing);
            _view.Show();

            _gameTime = new DispatcherTimer();
            _gameTime.Interval = TimeSpan.FromSeconds(1);
            _gameTime.Tick += new EventHandler(GameTime_Tick);
            _gameTime.Start();

            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromSeconds(2);
            _timer.Tick += new EventHandler(Timer_Tick);
            _timer.Start();

            _player = new SoundPlayer();
            _player.Stream = TowerDefense.Properties.Resources.music;
            _player.PlayLooping();
            _playerStopped = false;
        }
        #endregion

        #region Timer event handlers
        /// <summary>
        /// Advances game at every tick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            _model.Step();
        }

        /// <summary>
        /// Advances gametime at every tick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GameTime_Tick(object sender, EventArgs e)
        {
            _model.AdvanceTime();
        }
        #endregion

        #region View event handlers
        /// <summary>
        /// When we want to close the game, pops up a messagebox if we are sure that we want to close it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void View_Closing(object sender, CancelEventArgs e)
        {
            Boolean restartTimer = _timer.IsEnabled;

            _gameTime.Stop();
            _timer.Stop();

            if (MessageBox.Show("Are you sure you want to quit?", "AREA51", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
            {
                e.Cancel = true;

                if (restartTimer)
                {
                    _gameTime.Start();
                    _timer.Start();
                }
            }
        }
        #endregion

        #region ViewModel event handlers
        /// <summary>
        /// When we click on the newgame button, starts a new game with the current difficulty
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_NewGame(object sender, EventArgs e)
        {
            _model.NewGame(_model.Difficulty);
        }

        /// <summary>
        /// When we want to close the game, calls the Close function of the MainWindow
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_ExitGame(object sender, System.EventArgs e)
        {
            _view.Close();
        }

        /// <summary>
        /// When we press the pause button, stops the timers if they're running, or starts if they're not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PauseGame(object sender, System.EventArgs e)
        {
            if (_gameTime.IsEnabled)
            {
                _gameTime.Stop();
                _timer.Stop();
            }
            else
            {
                _gameTime.Start();
                _timer.Start();
            }
        }

        /// <summary>
        /// When we press the pause music button, stops the music if it's playing, or starts if it's not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PauseMusic(object sender, System.EventArgs e)
        {
            if (_playerStopped)
            {
                _player.PlayLooping();
                _playerStopped = false;
            }
            else
            {
                _player.Stop();
                _playerStopped = true;
            }
        }
        #endregion

        #region Model event handlers
        /// <summary>
        /// When the game is created, we adjust the timer according to the difficulty
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_GameCreated(object sender, GameEventArgs e)
        {
            if (_timer != null)
            {
                switch (_model.Difficulty)
                {
                    case GameDifficulty.Easy:
                        _timer.Interval = TimeSpan.FromSeconds(2);
                        break;
                    case GameDifficulty.Medium:
                        _timer.Interval = TimeSpan.FromSeconds(1.5);
                        break;
                    case GameDifficulty.Hard:
                        _timer.Interval = TimeSpan.FromSeconds(1);
                        break;
                }

                _gameTime.Start();
                _timer.Start();
            }
        }

        /// <summary>
        /// When the game is over, we display a message with the stats
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_GameOver(object sender, GameEventArgs e)
        {
            _gameTime.Stop();
            _timer.Stop();

            if (!e.IsWon)
            {
                MessageBox.Show("Game over! You lost!" + Environment.NewLine +
                                "Waves: " + (e.Waves - 1) + Environment.NewLine +
                                "Gametime: " + TimeSpan.FromSeconds(e.GameTime).ToString("g"),
                                "AREA51",
                                MessageBoxButton.OK,
                                MessageBoxImage.Asterisk);
            }
            else
            {
                MessageBox.Show("Game over! You won!" + Environment.NewLine +
                                "Waves: " + (e.Waves - 1) + Environment.NewLine +
                                "Gametime: " + TimeSpan.FromSeconds(e.GameTime).ToString("g"),
                                "AREA51",
                                MessageBoxButton.OK,
                                MessageBoxImage.Asterisk);
            }
        }
        #endregion
    }
}
