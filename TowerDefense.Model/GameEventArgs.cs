﻿using System;

namespace TowerDefense.Model
{
    /// <summary>
    /// GameEventArgs type
    /// </summary>
    public class GameEventArgs : EventArgs
    {
        private float _gameTime;
        private Int32 _waves;
        private Boolean _isWon;

        /// <summary>
        /// Get the time elapsed
        /// </summary>
        public float GameTime { get { return _gameTime; } }

        /// <summary>
        /// Get the number of waves
        /// </summary>
        public Int32 Waves { get { return _waves; } }

        /// <summary>
        /// Check if the game is won
        /// </summary>
        public Boolean IsWon { get { return _isWon; } }

        /// <summary>
        /// Constructor, initializes fields
        /// </summary>
        /// <param name="iswon">Did we win?</param>
        /// <param name="gametime">Time elapsed</param>
        /// <param name="waves">Waves survived</param>
        public GameEventArgs(Boolean iswon, float gametime, Int32 waves)
        {
            _gameTime = gametime;
            _isWon = iswon;
            _waves = waves;
        }
    }
}
