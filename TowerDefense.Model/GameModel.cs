﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TowerDefense.Model
{
    /// <summary>
    /// Enum for gamedifficulty
    /// </summary>
    public enum GameDifficulty { Easy, Medium, Hard };

    /// <summary>
    /// GameModel type
    /// </summary>
    public class GameModel
    {
        #region Fields
        private List<IUnit> _towers;
        private List<IUnit> _enemies;
        private List<IUnit> _mines;
        private Tuple<Int32, Int32> _tableSize;
        private Int32 _gold;
        private Int32 _ultimateProgress;
        private Int32 _time;
        private Int32 _waveCounter;
        private Int32 _waveTimer;
        private Int32 _stepCounter;
        private GameDifficulty _gameDifficulty;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor, first game is always easy
        /// </summary>
        public GameModel()
        {
            NewGame(GameDifficulty.Easy);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Starts a new game with the given difficulty
        /// </summary>
        /// <param name="difficulty">Game difficulty</param>
        public void NewGame(GameDifficulty difficulty)
        {
            _gameDifficulty = difficulty;

            switch (_gameDifficulty)
            {
                case GameDifficulty.Easy:
                    _gold = 800;
                    _tableSize = new Tuple<int, int>(30, 25);
                    break;
                case GameDifficulty.Medium:
                    _gold = 700;
                    _tableSize = new Tuple<int, int>(30, 30);
                    break;
                case GameDifficulty.Hard:
                    _gold = 600;
                    _tableSize = new Tuple<int, int>(30, 35);
                    break;
            }

            _time = 0;
            _ultimateProgress = 0;
            _waveCounter = 1;
            _waveTimer = 20;
            _stepCounter = 0;
            _towers = new List<IUnit>();
            _enemies = new List<IUnit>();
            _mines = new List<IUnit>();

            OnGameCreated();
        }

        /// <summary>
        /// Global step function
        /// </summary>
        public void Step()
        {
            // Enemies damage towers and mines
            foreach (IUnit enemy in _enemies)
            {
                var tmp = _towers.ToList<IUnit>();
                tmp.AddRange(_mines);
                enemy.Step(tmp);
            }

            // Check if any tower is dead
            foreach (IUnit tower in _towers.ToList<IUnit>())
            {
                if (tower.HP <= 0)
                {
                    _towers.Remove(tower);
                }
            }

            // Check if any mine is dead
            foreach (IUnit mine in _mines.ToList<IUnit>())
            {
                if (mine.HP <= 0)
                {
                    _mines.Remove(mine);
                }
            }

            // Towers damage enemies
            foreach (IUnit tower in _towers)
            {
                tower.Step(_enemies);
            }

            // Check if any enemy is dead
            foreach (IUnit enemy in _enemies.ToList<IUnit>())
            {
                if (enemy.HP <= 0)
                {
                    _enemies.Remove(enemy);
                    _gold += 1;
                    if (_ultimateProgress < 100)
                    {
                        _ultimateProgress += 10;
                    }
                }
            }

            MoveEnemies();
            SpawnEnemies();

            _stepCounter++;

            // Check if game is over
            Boolean gameOver = false;
            foreach (IUnit enemy in _enemies)
            {
                if (enemy.X < 0)
                {
                    OnGameOver(false);
                    gameOver = true;
                    break;
                }
            }

            if (_time >= 60 * 15)
            {
                OnGameOver(true);
                gameOver = true;
            }

            // Advance game if it's not over
            if (!gameOver)
            {
                OnGameAdvanced();
            }
        }

        /// <summary>
        /// Checks if the given position is free
        /// </summary>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <returns></returns>
        public Boolean IsFreePos(Int32 x, Int32 y)
        {
            foreach (Enemy enemy in _enemies)
            {
                if ((enemy.X == x) && (enemy.Y == y))
                {
                    return false;
                }
            }

            foreach (IUnit tower in _towers)
            {
                if ((tower.X == x) && (tower.Y == y))
                {
                    return false;
                }
            }

            foreach (Mine mine in _mines)
            {
                if ((mine.X == x) && (mine.Y == y))
                {
                    return false;
                }
            }

            if (x < 0 || x > TableSize.Item1 - 1 || y < 0 || y > TableSize.Item2 - 1)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Spawns a tower if the position is free
        /// </summary>
        /// <typeparam name="T">Type of the tower</typeparam>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="constructor">Constructor function</param>
        public void SpawnTower<T>(Int32 x, Int32 y, Func<Int32, Int32, T> constructor) where T : IUnit
        {
            if (IsFreePos(x, y))
            {
                IUnit item = constructor(x, y);
                if (_gold >= item.Price)
                {
                    if (item is Mine)
                    {
                        if (_mines.Count < 10)
                        {
                            _mines.Add(item);
                        }
                    }
                    else
                    {
                        _towers.Add(item);
                    }
                    _gold -= item.Price;
                }
            }
        }

        /// <summary>
        /// Upgrades tower on the given position
        /// </summary>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        public void Upgrade(Int32 x, Int32 y)
        {
            foreach (IUnit tower in _towers)
            {
                if (tower.X == x && tower.Y == y && _gold >= tower.Price && tower.Level < 3)
                {
                    tower.UpgradeTower();
                    _gold -= tower.Price;
                    return;
                }
            }
        }

        /// <summary>
        /// Spawns random enemies based on the wave count
        /// </summary>
        public void SpawnEnemies()
        {
            Random rand = new Random();
            Int32 coordinate = rand.Next(0, _tableSize.Item2);

            // If waveTimer is <= 0, then the wave starts
            if (_waveTimer <= 0)
            {
                // Spawns an enemy in the last row's random column
                if (_waveCounter <= 5 && _stepCounter % 3 == 0)
                {
                    _enemies.Add(new Enemy(_tableSize.Item1 - 1, coordinate));
                    return;
                }
                else if (_waveCounter > 5 && _waveCounter <= 10 && _stepCounter % 2 == 0)
                {
                    _enemies.Add(new Enemy(_tableSize.Item1 - 1, coordinate));
                    return;
                }
                else if (_waveCounter > 10)
                {
                    _enemies.Add(new Enemy(_tableSize.Item1 - 1, coordinate));
                    return;
                }
            }
        }

        /// <summary>
        /// Moves enemies upwards on the table
        /// </summary>
        private void MoveEnemies()
        {
            foreach (IUnit enemy in _enemies)
            {
                bool cannotMove = false;

                foreach (IUnit towersAndMines in _towers.Concat(_mines))
                {
                    if (towersAndMines.Y == enemy.Y && towersAndMines.X + 1 == enemy.X)
                    {
                        cannotMove = true;
                        break;
                    }
                }

                if (!cannotMove)
                {
                    enemy.X--;
                }
            }
        }

        /// <summary>
        /// Special ability which kills enemies in a cross pattern with (x,y) center
        /// </summary>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        public void Ultimate(Int32 x, Int32 y)
        {
            // If the ultimate is not ready, we return
            if (_ultimateProgress < 100)
            {
                return;
            }

            for (Int32 i = 0; i < _tableSize.Item1; i++)
            {
                for (Int32 j = Math.Max(y - 1, 0); j <= Math.Min(y + 1, _tableSize.Item1); ++j)
                {
                    RemoveUnit(i, j);
                }
            }

            for (Int32 i = 0; i < _tableSize.Item2; i++)
            {
                for (Int32 j = Math.Max(x - 1, 0); j <= Math.Min(x + 1, _tableSize.Item2); ++j)
                {
                    RemoveUnit(j, i);
                }
            }

            _ultimateProgress = 0;
        }

        /// <summary>
        /// Removes enemy from the given position if it's possible
        /// </summary>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        public void RemoveUnit(Int32 x, Int32 y)
        {
            // Removes unit if exists
            var enemy = _enemies.SingleOrDefault(s => (s.X == x) && (s.Y == y));
            if (enemy != null)
            {
                _enemies.Remove(enemy);
            }
        }

        /// <summary>
        /// Removes tower from the given position if it's possible
        /// </summary>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        public void RemoveTower(Int32 x, Int32 y)
        {
            // Removes the tower with the given params from the list, if exists
            var mine = _mines.SingleOrDefault(s => (s.X == x) && (s.Y == y));
            if (mine != null)
            {
                _mines.Remove(mine);
                _gold += mine.Price / 2;
                return;
            }

            var tower = _towers.SingleOrDefault(s => (s.X == x) && (s.Y == y));
            if (tower != null)
            {
                _towers.Remove(tower);
                _gold += tower.Price / 2;
                return;
            }
        }

        /// <summary>
        /// Advances time, wavetimer, wavecounter and adds gold
        /// </summary>
        public void AdvanceTime()
        {
            //The time between 0 and -40 is the duration of the wave
            if (_waveTimer > -40)
            {
                _waveTimer--;

                if (_waveTimer == 0)
                {
                    _stepCounter = 0;
                }
            }
            else
            {
                //If the wavetime falls below -40, wave is cancelled and timer resetted
                _waveCounter++;
                _waveTimer = 19;
            }

            if (_waveTimer <= 0)
            {
                _gold += 1 + _mines.Count;
            }

            _time++;

            OnGameTimeAdvanced();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Get the game difficulty
        /// </summary>
        public GameDifficulty Difficulty { get { return _gameDifficulty; } }

        /// <summary>
        /// Get the table size
        /// </summary>
        public Tuple<Int32, Int32> TableSize { get { return _tableSize; } }

        /// <summary>
        /// Get the game time
        /// </summary>
        public Int32 Time { get { return _time; } }

        /// <summary>
        /// Get the gold
        /// </summary>
        public Int32 Gold { get { return _gold; } }

        /// <summary>
        /// Get the wave count
        /// </summary>
        public Int32 WaveCount { get { return _waveCounter; } }

        /// <summary>
        /// Get the wave timer
        /// </summary>
        public Int32 WaveTimer { get { return _waveTimer; } }

        /// <summary>
        /// Get or set the ultimate progress
        /// </summary>
        public Int32 UltimateProgress { get { return _ultimateProgress; } set { _ultimateProgress = value; } }

        /// <summary>
        /// Get the list of towers
        /// </summary>
        public List<IUnit> Towers { get { return _towers; } }

        /// <summary>
        /// Get the list of enemies
        /// </summary>
        public List<IUnit> Enemies { get { return _enemies; } }

        /// <summary>
        /// Get the list of mines
        /// </summary>
        public List<IUnit> Mines { get { return _mines; } }
        #endregion

        #region Events
        /// <summary>
        /// Event for gameadvanced
        /// </summary>
        public event EventHandler<GameEventArgs> GameAdvanced;

        /// <summary>
        /// Event for gameover
        /// </summary>
        public event EventHandler<GameEventArgs> GameOver;

        /// <summary>
        /// Event for gamecreated
        /// </summary>
        public event EventHandler<GameEventArgs> GameCreated;

        /// <summary>
        /// Event for gametimeadvanced
        /// </summary>
        public event EventHandler<GameEventArgs> GameTimeAdvanced;
        #endregion

        #region Private event methods
        /// <summary>
        /// Event handler for game advance
        /// </summary>
        private void OnGameAdvanced()
        {
            if (GameAdvanced != null)
            {
                GameAdvanced(this, new GameEventArgs(false, _time, _waveCounter));
            }
        }

        /// <summary>
        /// Event handler for game over
        /// </summary>
        /// <param name="isWon">game status</param>
        private void OnGameOver(Boolean isWon)
        {
            _ultimateProgress = 0;

            if (GameOver != null)
            {
                GameOver(this, new GameEventArgs(isWon, _time, _waveCounter));
            }
        }

        /// <summary>
        /// Event handler for creating the game
        /// </summary>
        private void OnGameCreated()
        {
            if (GameCreated != null)
            {
                GameCreated(this, new GameEventArgs(false, _time, _waveCounter));
            }
        }

        /// <summary>
        /// Event handler for game time advance
        /// </summary>
        private void OnGameTimeAdvanced()
        {
            if (GameTimeAdvanced != null)
            {
                GameTimeAdvanced(this, new GameEventArgs(false, _time, _waveCounter));
            }
        }
        #endregion
    }
}