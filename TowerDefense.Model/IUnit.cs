﻿using System;
using System.Collections.Generic;

namespace TowerDefense.Model
{
    /// <summary>
    /// Abstarct Class for Units eg. Tower, Enemy, Mine...
    /// </summary>
    public abstract class IUnit
    {
        protected Int32 _x;
        protected Int32 _y;
        protected Int32 _healthPoint;
        protected Int32 _maxHealthPoint;
        protected Int32 _level;
        protected Int32 _price;
        protected Tuple<Int32, Int32> _range;

        /// <summary>
        /// Virtual Step method for units 
        /// Which is called in every 3 seconds of the game
        /// </summary>
        /// <param name="list">List of units</param>
        public virtual void Step(List<IUnit> list) { }

        /// <summary>
        /// Virtual Upgrade method for Towers
        /// Enemies and Mines doesn't override this function
        /// </summary>
        public virtual void UpgradeTower() {}

        #region Properties
        /// <summary>
        /// Getter and setter for x coordinate
        /// </summary>
        public Int32 X { get { return _x; } set { _x = value; } }

        /// <summary>
        /// Getter and setter for Y coordinate
        /// </summary>
        public Int32 Y { get { return _y; } set { _y = value; } }

        /// <summary>
        /// Getter and setter for the healthpoint of the Enemy
        /// </summary>
        public Int32 HP { get { return _healthPoint; } set { _healthPoint = value; } }

        /// <summary>
        /// Getter for maximal healthpoint value (use only by enemies)
        /// </summary>
        public Int32 MaxHP { get { return _maxHealthPoint; } }

        /// <summary>
        /// Getter for price (use only by towers and mines)
        /// </summary>
        public Int32 Price { get { return _price; } }

        /// <summary>
        /// Getter for Level (use only by towers)
        /// </summary>
        public Int32 Level { get { return _level; } }

        /// <summary>
        /// Getter of the range of a tower (use only by towers)
        /// the height and width of a range can be different
        /// first coord for height
        /// second coord for width
        /// </summary>
        public Tuple<Int32, Int32> Range { get { return _range; } }
        #endregion
    };

    /// <summary>
    /// Derived class for Enemies
    /// </summary>
    public class Enemy : IUnit
    {
        /// <summary>
        /// Constructor, for the Enemy Class
        /// </summary>
        /// <param name="x"> x coordinate of the Enemy</param>
        /// <param name="y"> y coordinate of the Enemy</param>
        public Enemy(Int32 x, Int32 y)
        {
            _x = x;
            _y = y;
            _maxHealthPoint = 1;
            _healthPoint = _maxHealthPoint;
        }

        /// <summary>
        /// See comment in the base class (IUnit)
        /// </summary>
        public override void Step(List<IUnit> mines_towers)
        {
            foreach(IUnit mineOrTower in mines_towers)
            {
                // Damage tower
                if (_x == mineOrTower.X + 1 && _y == mineOrTower.Y)
                {
                    mineOrTower.HP--;
                }
            }
        }
    }

    /// <summary>
    /// Derived class for T1 
    /// which is a Tower type
    /// </summary>
    public class T1 : IUnit
    {
        private Int32 _firingRate = 1;
        private Int32 _targetNumber = 1;
        private Int32 _timeCounter = 0;
        private Int32 _targetCounter = 0;

        /// <summary>
        /// Constructor for the T1 Class
        /// </summary>
        /// <param name="x">x coordinate of the Tower</param>
        /// <param name="y">y coordinate of the Tower</param>
        public T1(Int32 x, Int32 y)
        {
            _x = x;
            _y = y;
            _maxHealthPoint = 3;
            _healthPoint = _maxHealthPoint;
            _range = new Tuple<Int32, Int32> (1,1);
            _price = 100;
            _level = 1;
        }

        /// <summary>
        /// See comment in the base class (IUnit)
        /// </summary>
        public override void Step(List<IUnit> enemies)
        {
            _timeCounter++;

            if (_timeCounter % _firingRate == 0)
            {
                foreach (IUnit enemy in enemies)
                {
                    if ((Math.Abs(enemy.X - _x) <= _range.Item1) && (Math.Abs(enemy.Y - _y) <= _range.Item2))
                    {
                        enemy.HP--;
                        _targetCounter++;

                        if (_targetNumber == _targetCounter)
                        {
                            _targetCounter = 0;
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// See comment on the base class (IUnit)
        /// </summary>
        public override void UpgradeTower()
        {
            if (_level < 3)
            {
                _level++;

                if(_level == 2)
                {
                    _healthPoint++;
                    _maxHealthPoint++;
                    _targetNumber = 2;
                    _range = new Tuple<Int32, Int32>(1, 2);
                }
                else
                {
                    _healthPoint++;
                    _maxHealthPoint++;
                    _firingRate = 2;
                    _range = new Tuple<Int32, Int32>(2, 2);
                }
            }
        }
    }

    /// <summary>
    /// Derived class for T2
    /// which is a Tower type
    /// </summary>
    public class T2 : IUnit
    {
        private Int32 _firingRate = 2;
        private Int32 _targetNumber = 1;
        private Int32 _timeCounter = 0;
        private Int32 _targetCounter = 0;

        /// <summary>
        /// Constructor for the T2 Class
        /// </summary>
        /// <param name="x">x coordinate of the Tower</param>
        /// <param name="y">y coordinate of the Tower</param>
        public T2(Int32 x, Int32 y)
        {
            _x = x;
            _y = y;
            _maxHealthPoint = 2;
            _healthPoint = _maxHealthPoint;
            _range = new Tuple<Int32, Int32>(2, 0);
            _price = 25;
            _level = 1;
        }

        /// <summary>
        /// See comment in the base class (IUnit)
        /// </summary>
        public override void Step(List<IUnit> enemies) 
        {
            _timeCounter++;

            if (_timeCounter % _firingRate == 0)
            {
                foreach (IUnit enemy in enemies)
                {
                    if ((Math.Abs(enemy.X - _x) <= _range.Item1) && (Math.Abs(enemy.Y - _y) <= _range.Item2))
                    {
                        enemy.HP--;
                        _targetCounter++;

                        if(_targetNumber == _targetCounter)
                        {
                            _targetCounter = 0;
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// See comment in the base class (IUnit)
        /// </summary>
        public override void UpgradeTower()
        {
            if (_level < 3)
            {
                _level++;

                if (_level == 2)
                {
                    _healthPoint += 2;
                    _maxHealthPoint += 2;
                }
                else
                {
                    _range = new Tuple<Int32, Int32>(3, 0);
                    _firingRate = 3;
                }
            }
        }
    }

    /// <summary>
    /// Derived class for T3
    /// which is a Tower type
    /// </summary>
    public class T3 : IUnit
    {
        private Int32 _firingRate = 1;
        private Int32 _targetNumber = 1;
        private Int32 _timeCounter = 0;
        private Int32 _targetCounter = 0;

        /// <summary>
        /// Constructor, for the T3 Class
        /// </summary>
        /// <param name="x">x coordinate of the Tower</param>
        /// <param name="y">y coordinate of the Tower</param>
        public T3(Int32 x, Int32 y)
        {
            _x = x;
            _y = y;
            _maxHealthPoint = 4;
            _healthPoint = _maxHealthPoint;
            _range = new Tuple<Int32, Int32>(0, 2);
            _price = 50;
            _level = 1;
        }

        /// <summary>
        /// See comment in the base class (IUnit)
        /// </summary>
        public override void Step(List<IUnit> enemies)
        {
            _timeCounter++;

            if (_timeCounter % _firingRate == 0)
            {
                foreach (IUnit enemy in enemies)
                {
                    if ((Math.Abs(enemy.X - _x) <= _range.Item1) && (Math.Abs(enemy.Y - _y) <= _range.Item2))
                    {
                        enemy.HP--;
                        _targetCounter++;

                        if (_targetNumber == _targetCounter)
                        {
                            _targetCounter = 0;
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// See comment in the base class (IUnit)
        /// </summary>
        public override void UpgradeTower()
        {
            if (_level < 3)
            {
                _level++;

                if (_level == 2)
                {
                    _range = new Tuple<Int32, Int32>(0, 3);
                }
                else
                {
                    _healthPoint++;
                    _maxHealthPoint++;
                }

            }
        }
    }

    /// <summary>
    /// Derived Class for Mine, which is a special type of Towers
    /// doesn't override the UpgradeTower() function
    /// </summary>
    public class Mine : IUnit
    {
        /// <summary>
        /// Constructor for the Mine Class
        /// </summary>
        /// <param name="x">x coordinate of the Mine (Tower)</param>
        /// <param name="y">y coordinate of the Mine (Tower)</param>
        public Mine(Int32 x, Int32 y)
        {
            _x = x;
            _y = y;
            _maxHealthPoint = 1;
            _healthPoint = _maxHealthPoint;
            _price = 25;
            _range = new Tuple<Int32, Int32>(1, 2);
        }
    }
}
