# CSharper Than You

CSharper Than You was the name of our team at the Software Technology course where we have been working in teams of 3 students and had to create a game based on the popular Tower Defense game but with creative themes. We got points for different implementations, such as difficulty levels, AI enemies, multi-level characters etc. In this course we had to work in teams and to use GitLab. We also had weekly scrum meetings with our tutor which was also great.

In my team I was responsible mostly for the implementation of the Game Model(game logic). My other teammates did the View and the statistics. For further information and the full documentation read the WIKI. I think we did a very great job and I had a good time working with others and our tutor said our project was superior to ther teams projects.

I advise you to have a try with this game :) 
