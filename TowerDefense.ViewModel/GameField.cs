﻿using System;

namespace TowerDefense.ViewModel
{
    /// <summary>
    /// GameField type
    /// </summary>
    public class GameField : ViewModelBase
    {
        private String _content;
        private String _background;
        private Boolean _isLocked;
        private Int32 _healthPoint;
        private Int32 _maxHealthPoint;

        #region Properties
        /// <summary>
        /// Get the content of the field
        /// </summary>
        public String Content
        {
            get { return _content; }
            set
            {
                if (_content != value)
                {
                    _content = value;
                    OnPropertyChanged("Content");
                }
            }
        }

        /// <summary>
        /// Get the background of the field
        /// </summary>
        public String Background
        {
            get { return _background; }
            set
            {
                if (_background != value)
                {
                    _background = value;
                    OnPropertyChanged("Background");
                }
            }
        }

        /// <summary>
        /// Check if the field is locked
        /// </summary>
        public Boolean IsLocked
        {
            get { return _isLocked; }
            set
            {
                if (_isLocked != value)
                {
                    _isLocked = value;
                    OnPropertyChanged("IsLocked");
                }
            }
        }

        /// <summary>
        /// Get the hp of the field
        /// </summary>
        public Int32 HP
        {
            get { return _healthPoint; }
            set
            {
                if (_healthPoint != value)
                {
                    _healthPoint = value;
                    OnPropertyChanged("HP");
                    OnPropertyChanged("HasUnit");
                }
            }
        }

        /// <summary>
        /// Get the maxhp of the field
        /// </summary>
        public Int32 MaxHP
        {
            get { return _maxHealthPoint; }
            set
            {
                if (_maxHealthPoint != value)
                {
                    _maxHealthPoint = value;
                    OnPropertyChanged("MaxHP");
                }
            }
        }

        /// <summary>
        /// Check if the field has a unit on it
        /// </summary>
        public Boolean HasUnit { get { return _healthPoint > 0; } }

        /// <summary>
        /// Get the x coordinate of the field
        /// </summary>
        public Int32 X { get; set; }

        /// <summary>
        /// Get the y coordinate of the field
        /// </summary>
        public Int32 Y { get; set; }

        /// <summary>
        /// Get the number of the field
        /// </summary>
        public Int32 Number { get; set; }

        /// <summary>
        /// Get the left click command of the field
        /// </summary>
        public DelegateCommand StepCommand { get; set; }

        /// <summary>
        /// Get the right click command of the field
        /// </summary>
        public DelegateCommand DeleteTowerCommand { get; set; }
        #endregion
    }
}
