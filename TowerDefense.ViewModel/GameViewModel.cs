﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TowerDefense.Model;

namespace TowerDefense.ViewModel
{
    /// <summary>
    /// Enum for gametable action
    /// </summary>
    public enum Mode { Spawn, Upgrade, Ultimate };

    /// <summary>
    /// Enum for spawning a certain type of tower
    /// </summary>
    public enum SpawnType { Mine, T1, T2, T3, None };

    /// <summary>
    /// ViewModel type
    /// </summary>
    public class GameViewModel : ViewModelBase
    {
        #region Fields
        private GameModel _model;
        private Mode _mode;
        private SpawnType _spawnType;
        private Int32 _resolution;
        private Boolean _ultimateUsed;
        private List<Tuple<Int32, Int32>> _makeBlank;
        #endregion

        #region Properties
        /// <summary>
        /// Get the new game command
        /// </summary>
        public DelegateCommand NewGameCommand { get; private set; }

        /// <summary>
        /// Get the exit command
        /// </summary>
        public DelegateCommand ExitCommand { get; private set; }

        /// <summary>
        /// Get the pause command
        /// </summary>
        public DelegateCommand PauseGameCommand { get; private set; }

        /// <summary>
        /// Get the pause music command
        /// </summary>
        public DelegateCommand PauseMusicCommand { get; private set; }

        /// <summary>
        /// Get the quick select tower command
        /// </summary>
        public DelegateCommand QuickSelectTowerCommand { get; private set; }

        /// <summary>
        /// Get the ultimate command
        /// </summary>
        public DelegateCommand UltimateCommand { get; private set; }

        /// <summary>
        /// Get the gamefields
        /// </summary>
        public ObservableCollection<GameField> GameFields { get; set; }

        /// <summary>
        /// Get the towerbuttons for the top panel
        /// </summary>
        public ObservableCollection<GameField> TowerButtons { get; set; }

        /// <summary>
        /// Get the vertical tablesize
        /// </summary>
        public Int32 TableSizeX { get { return _model.TableSize.Item1 + 2; } }

        /// <summary>
        /// Get the horizontal tablesize
        /// </summary>
        public Int32 TableSizeY { get { return _model.TableSize.Item2; } }

        /// <summary>
        /// Get the amount of gold
        /// </summary>
        public Int32 GameGold { get { return _model.Gold; } }

        /// <summary>
        /// Get the elapsed time
        /// </summary>
        public String GameTime { get { return ((int)_model.Time).ToString() + " s"; } }

        /// <summary>
        /// Get the number of waves
        /// </summary>
        public Int32 GameWave { get { return _model.WaveCount; } }

        /// <summary>
        /// Get the time remaining until the next wave
        /// </summary>
        public Int32 GameWaveTimeRemaining
        {
            get
            {
                if (_model.WaveTimer >= 0)
                {
                    return _model.WaveTimer;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Get the ultimate progress
        /// </summary>
        public Int32 UltimateProgress { get { return _model.UltimateProgress; } }

        /// <summary>
        /// Check if the ultimate is enabled
        /// </summary>
        public Boolean UltimateEnabled { get { return _model.UltimateProgress >= 100; } }

        /// <summary>
        /// Check if the game is easy, if it's not set it to easy
        /// </summary>
        public Boolean IsGameEasy
        {
            get { return _model.Difficulty == GameDifficulty.Easy; }
            set
            {
                if (_model.Difficulty == GameDifficulty.Easy)
                {
                    return;
                }

                _model.NewGame(GameDifficulty.Easy);

                DifficultyChangeUpdate();
            }
        }

        /// <summary>
        /// Check if the game is medium, if it's not set it to medium
        /// </summary>
        public Boolean IsGameMedium
        {
            get { return _model.Difficulty == GameDifficulty.Medium; }
            set
            {
                if (_model.Difficulty == GameDifficulty.Medium)
                {
                    return;
                }

                _model.NewGame(GameDifficulty.Medium);

                DifficultyChangeUpdate();
            }
        }

        /// <summary>
        /// Check if the game is hard, if it's not set it to hard
        /// </summary>
        public Boolean IsGameHard
        {
            get { return _model.Difficulty == GameDifficulty.Hard; }
            set
            {
                if (_model.Difficulty == GameDifficulty.Hard)
                {
                    return;
                }

                _model.NewGame(GameDifficulty.Hard);

                DifficultyChangeUpdate();
            }
        }

        /// <summary>
        /// Get the vertical resolution of the gametable
        /// </summary>
        public Int32 TableResX { get { return (_model.TableSize.Item2 * _resolution - 500) / 2; } }

        /// <summary>
        /// Get the horizontal resolution of the gametable
        /// </summary>
        public Int32 TableResY { get { return _model.TableSize.Item1 * _resolution; } }

        /// <summary>
        /// Check if the full hd resolution is enabled
        /// </summary>
        public Boolean IsFHDEnabled { get; private set; }

        /// <summary>
        /// Check if the 4k resolution is enabled
        /// </summary>
        public Boolean Is4KEnabled { get; private set; }

        /// <summary>
        /// Check if the resolution is hd, if it's not then set it to hd
        /// </summary>
        public Boolean IsResolutionHD
        {
            get { return _resolution == 20; }
            set
            {
                if (_resolution == 20)
                {
                    return;
                }

                _resolution = 20;

                ResolutionChangeUpdate();
            }
        }

        /// <summary>
        /// Check if the resolution is full hd, if it's not then set it to full hd
        /// </summary>
        public Boolean IsResolutionFHD
        {
            get { return _resolution == 30; }
            set
            {
                if (_resolution == 30)
                {
                    return;
                }

                _resolution = 30;

                ResolutionChangeUpdate();
            }
        }

        /// <summary>
        /// Check if the resolution is 4k, if it's not then set it to 4k
        /// </summary>
        public Boolean IsResolution4K
        {
            get { return _resolution == 65; }
            set
            {
                if (_resolution == 65)
                {
                    return;
                }

                _resolution = 65;

                ResolutionChangeUpdate();
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Event for starting a new game
        /// </summary>
        public event EventHandler NewGame;

        /// <summary>
        /// Event for exitting the game
        /// </summary>
        public event EventHandler ExitGame;

        /// <summary>
        /// Event for pausing the game
        /// </summary>
        public event EventHandler PauseGame;

        /// <summary>
        /// Event for pausing the music
        /// </summary>
        public event EventHandler PauseMusic;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor, initializes fields and connects events and eventhandlers
        /// </summary>
        /// <param name="model">Gamemodel</param>
        public GameViewModel(GameModel model)
        {
            _model = model;
            _model.GameAdvanced += new EventHandler<GameEventArgs>(Model_GameAdvanced);
            _model.GameCreated += new EventHandler<GameEventArgs>(Model_GameCreated);
            _model.GameOver += new EventHandler<GameEventArgs>(Model_GameOver);
            _model.GameTimeAdvanced += new EventHandler<GameEventArgs>(Model_GameTimeAdvanced);

            //Setting variables to their default values
            _mode = Mode.Upgrade;
            _spawnType = SpawnType.None;
            _ultimateUsed = false;
            _makeBlank = new List<Tuple<Int32, Int32>>();

            //Setting default resolution (depends on System Resolution)
            SetDefaultResolution();

            //Setup button commands
            NewGameCommand = new DelegateCommand(param => OnNewGame());
            ExitCommand = new DelegateCommand(param => OnExitGame());
            PauseGameCommand = new DelegateCommand(param => OnPauseGame());
            PauseMusicCommand = new DelegateCommand(param => OnPauseMusic());

            QuickSelectTowerCommand = new DelegateCommand(param => SelectTower(Convert.ToInt32(param)));
            UltimateCommand = new DelegateCommand(param => SelectUltimate());

            //Setup tower selection buttons
            TowerButtons = new ObservableCollection<GameField>();
            for (Int32 i = 0; i < 5; ++i)
            {
                TowerButtons.Add(new GameField
                {
                    IsLocked = false,
                    Number = i,
                    StepCommand = new DelegateCommand(param => SelectTower(Convert.ToInt32(param)))
                });
            }
            SetTowerButtonContent();

            //Setup gametable
            GameFields = new ObservableCollection<GameField>();
            GenerateTable();
        }
        #endregion

        #region Game methods
        /// <summary>
        /// Fills gametable according to the tablesize
        /// </summary>
        public void GenerateTable()
        {
            GameFields.Clear();
            for (Int32 i = 0; i < TableSizeX; i++)
            {
                String background = "#13121b";
                Boolean islocked = false;

                //Our base is blue(ish), enemy base is red(ish)
                if (i == 0)
                {
                    background = "#41ead4";
                    islocked = true;
                }
                else if (i == TableSizeX - 1)
                {
                    background = "#ff206e";
                    islocked = true;
                }

                for (Int32 j = 0; j < TableSizeY; j++)
                {
                    GameFields.Add(new GameField
                    {
                        Content = String.Empty,
                        Background = background,
                        IsLocked = islocked,
                        HP = 0,
                        X = i,
                        Y = j,
                        Number = i * TableSizeY + j,
                        StepCommand = new DelegateCommand(param => Step(Convert.ToInt32(param))),
                        DeleteTowerCommand = new DelegateCommand(param => DeleteTower(Convert.ToInt32(param)))
                    });
                }
            }
        }

        /// <summary>
        /// Refreshes the gametable (enemies, towers, stats, etc.)
        /// </summary>
        public void RefreshTable()
        {
            if (!_ultimateUsed)
            {
                //If we didn't use ultimate, make previous enemy positions invisible
                foreach (Tuple<Int32, Int32> pos in _makeBlank)
                {
                    GameFields[pos.Item1 * TableSizeY + pos.Item2].Content = String.Empty;
                    GameFields[pos.Item1 * TableSizeY + pos.Item2].HP = 0;
                }
            }
            else
            {
                //If we used the ultimate we repaint everything
                foreach (GameField field in GameFields)
                {
                    if (field.X != 0 && field.X != TableSizeX - 1)
                    {
                        field.Background = "#13121b";
                        field.Content = String.Empty;
                        field.HP = 0;
                    }
                }

                _ultimateUsed = false;
            }

            //Clear the previous positions, so we have to make less fields blank in the next rotation
            _makeBlank.Clear();

            foreach (IUnit enemy in _model.Enemies)
            {
                //If the enemy killed the tower, make the range invisible
                if (GameFields[(enemy.X + 1) * TableSizeY + enemy.Y].Background == "#23222a" && GameFields[(enemy.X + 1) * TableSizeY + enemy.Y].Content.Contains(".gif"))
                {
                    DisplayTowerRange(enemy, 3, 3, "#13121b");
                }

                //Show the enemy's new position
                GameFields[(enemy.X + 1) * TableSizeY + enemy.Y].Content = "Content/enemy.gif";
                GameFields[(enemy.X + 1) * TableSizeY + enemy.Y].HP = enemy.HP;
                GameFields[(enemy.X + 1) * TableSizeY + enemy.Y].MaxHP = enemy.MaxHP;

                //Add current enemy position to the list
                _makeBlank.Add(new Tuple<Int32, Int32>(enemy.X + 1, enemy.Y));
            }

            //Show all the towers
            RefreshTowers();

            //Show all the mines
            foreach (IUnit mine in _model.Mines)
            {
                GameFields[(mine.X + 1) * TableSizeY + mine.Y].Content = "Content/mine.png";
                GameFields[(mine.X + 1) * TableSizeY + mine.Y].HP = mine.HP;
                GameFields[(mine.X + 1) * TableSizeY + mine.Y].MaxHP = mine.MaxHP;
            }
        }

        /// <summary>
        /// Refreshes the towers on the gametable
        /// </summary>
        private void RefreshTowers()
        {
            foreach (IUnit tower in _model.Towers)
            {
                DisplayTowerRange(tower, tower.Range.Item1, tower.Range.Item2, "#23222a");

                SetTowerImage(tower);

                GameFields[(tower.X + 1) * TableSizeY + tower.Y].HP = tower.HP;
                GameFields[(tower.X + 1) * TableSizeY + tower.Y].MaxHP = tower.MaxHP;
            }
        }

        /// <summary>
        /// Select which type of tower we would like to spawn
        /// </summary>
        /// <param name="index">Number of the gamefield</param>
        private void SelectTower(Int32 index)
        {
            //Select which type of tower we want to spawn
            GameField selectedButton = TowerButtons[index];

            if (selectedButton.IsLocked)
            {
                return;
            }

            if (selectedButton.Number != TowerButtons.Count - 1)
            {
                selectedButton.IsLocked = false;
                foreach (GameField button in TowerButtons)
                {
                    if (button != selectedButton && button.Number != TowerButtons.Count - 1)
                    {
                        button.IsLocked = true;
                    }
                }

                //Set spawntype and the mode to spawn
                _mode = Mode.Spawn;
                _spawnType = (SpawnType)selectedButton.Number;
            }
            else
            {
                //If we press X reset everything
                _mode = Mode.Upgrade;
                _spawnType = SpawnType.None;
                UnlockTowerButtons();
            }
        }

        /// <summary>
        /// Sets mode to ultimate
        /// </summary>
        public void SelectUltimate()
        {
            // A second check is needed in order to avoid displaying ultimate on the gametable when it's not ready
            if (_model.UltimateProgress != 100)
            {
                return;
            }

            _mode = Mode.Ultimate;
        }

        /// <summary>
        /// Does a certain action on the gametable according to the mode
        /// </summary>
        /// <param name="index">Number of the gamefield</param>
        private void Step(Int32 index)
        {
            GameField field = GameFields[index];

            switch (_mode)
            {
                case Mode.Spawn:
                    //If the mode is set to spawn, we spawn the selected type, refresh the table and set things to default
                    SpawnTower(field.X - 1, field.Y);
                    _mode = Mode.Upgrade;
                    RefreshTable();
                    UnlockTowerButtons();
                    break;
                case Mode.Ultimate:
                    //If the mode is set to ultimate, we use the ultimate, display it and tell the viewmodel that in the next rotation we have to refresh the whole table
                    _model.Ultimate(field.X - 1, field.Y);
                    _mode = Mode.Upgrade;
                    _ultimateUsed = true;
                    DisplayUltimate(field.X, field.Y);
                    break;
                case Mode.Upgrade:
                    //If the mode is set to upgrade, we upgrade the tower and refresh the table
                    _model.Upgrade(field.X - 1, field.Y);
                    RefreshTable();
                    UnlockTowerButtons();
                    break;
            }

            UpdateVisuals();
        }

        /// <summary>
        /// Spawn a tower depending on the spawntype and set it back to none when it's ready
        /// </summary>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordiante</param>
        private void SpawnTower(Int32 x, Int32 y)
        {
            switch (_spawnType)
            {
                case SpawnType.Mine:
                    _model.SpawnTower<Mine>(x, y, (i, j) => new Mine(i, j));
                    break;
                case SpawnType.T1:
                    _model.SpawnTower<T1>(x, y, (i, j) => new T1(i, j));
                    break;
                case SpawnType.T2:
                    _model.SpawnTower<T2>(x, y, (i, j) => new T2(i, j));
                    break;
                case SpawnType.T3:
                    _model.SpawnTower<T3>(x, y, (i, j) => new T3(i, j));
                    break;
            }

            _spawnType = SpawnType.None;
        }

        /// <summary>
        /// Deletes the tower on the field where we click
        /// </summary>
        /// <param name="index">Number of the gamefield</param>
        private void DeleteTower(Int32 index)
        {
            GameField field = GameFields[index];

            _model.RemoveTower(field.X - 1, field.Y);

            //Create a temporary mine to easily clear the range of the tower
            DisplayTowerRange(new Mine(field.X - 1, field.Y), 3, 3, "#13121b");
            field.Content = String.Empty;
            field.HP = 0;

            //Refresh the towers in case the range of any is affected and update the gold to show the income
            RefreshTowers();
            UpdateVisuals();
        }

        /// <summary>
        /// Unlocks available tower buttons
        /// </summary>
        private void UnlockTowerButtons()
        {
            for (int i = 1; i < 4; ++i)
            {
                TowerButtons[i].IsLocked = _model.Gold < TowerButtons[i].HP;
            }

            TowerButtons[0].IsLocked = _model.Gold < TowerButtons[0].HP || _model.Mines.Count >= 10;
        }

        /// <summary>
        /// If mode is not spawn unlocks available tower buttons
        /// </summary>
        private void ControlTowerButtons()
        {
            if (_mode != Mode.Spawn)
            {
                UnlockTowerButtons();
            }
        }

        /// <summary>
        /// Displays the range of the tower
        /// </summary>
        /// <param name="tower">Tower in the model</param>
        /// <param name="height">Height of the displayed area</param>
        /// <param name="width">Width of the displayed area</param>
        /// <param name="color">Color of the background</param>
        private void DisplayTowerRange(IUnit tower, Int32 height, Int32 width, String color)
        {
            for (int i = Math.Max(tower.X - height, 0); i <= Math.Min(tower.X + height, _model.TableSize.Item1 - 1); ++i)
            {
                for (int j = Math.Max(tower.Y - width, 0); j <= Math.Min(tower.Y + width, _model.TableSize.Item2 - 1); ++j)
                {
                    GameFields[(i + 1) * TableSizeY + j].Background = color;
                }
            }
        }

        /// <summary>
        /// Displays the range of the ultimate
        /// </summary>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        private void DisplayUltimate(Int32 x, Int32 y)
        {
            foreach (GameField field in GameFields)
            {
                if ((field.X == x || field.Y == y || field.X == x - 1 || field.Y == y - 1 || field.X == x + 1 || field.Y == y + 1) && field.X != 0 && field.X != TableSizeX - 1)
                {
                    field.Content = String.Empty;
                    field.Background = "#fbff12";
                    field.HP = 0;
                }
            }
        }

        /// <summary>
        /// Sets the tower image, according to its type and level
        /// </summary>
        /// <param name="tower">Tower in the model</param>
        private void SetTowerImage(IUnit tower)
        {
            if (tower is T1)
            {
                GameFields[(tower.X + 1) * TableSizeY + tower.Y].Content = "Content/ultra_space_station_lvl" + tower.Level.ToString() + ".gif";
            }
            else if (tower is T2)
            {
                GameFields[(tower.X + 1) * TableSizeY + tower.Y].Content = "Content/space_ship_lvl" + tower.Level.ToString() + ".gif";
            }
            else
            {
                GameFields[(tower.X + 1) * TableSizeY + tower.Y].Content = "Content/space_station_lvl" + tower.Level.ToString() + ".gif";
            }
        }

        /// <summary>
        /// Disables every button
        /// </summary>
        /// <param name="l">Disable/enable</param>
        private void LockControls(Boolean l)
        {
            foreach (GameField towerButton in TowerButtons)
            {
                towerButton.IsLocked = l;
            }

            foreach (GameField field in GameFields)
            {
                field.IsLocked = l;
            }
        }

        /// <summary>
        /// Update stats
        /// </summary>
        private void UpdateVisuals()
        {
            OnPropertyChanged("GameTime");
            OnPropertyChanged("GameGold");
            OnPropertyChanged("UltimateProgress");
            OnPropertyChanged("UltimateEnabled");
            OnPropertyChanged("GameWaveTimeRemaining");
            OnPropertyChanged("GameWave");
        }

        /// <summary>
        /// Update difficulty buttons and tablesize when the difficulty is changed
        /// </summary>
        private void DifficultyChangeUpdate()
        {
            //Update difficulty buttons
            OnPropertyChanged("IsGameEasy");
            OnPropertyChanged("IsGameMedium");
            OnPropertyChanged("IsGameHard");

            //Update tablesize
            OnPropertyChanged("TableSizeX");
            OnPropertyChanged("TableSizeY");

            //Update resolution
            OnPropertyChanged("TableResX");
            OnPropertyChanged("TableResY");
        }

        /// <summary>
        /// Updates resolution buttons
        /// </summary>
        private void ResolutionChangeUpdate()
        {
            //Update resolution buttons
            OnPropertyChanged("IsResolutionHD");
            OnPropertyChanged("IsResolutionFHD");
            OnPropertyChanged("IsResolution4K");

            //Update resolution
            OnPropertyChanged("TableResX");
            OnPropertyChanged("TableResY");
        }

        /// <summary>
        /// Sets resolution according to the system resolution
        /// </summary>
        private void SetDefaultResolution()
        {
            IsFHDEnabled = true;
            Is4KEnabled = true;

            if (System.Windows.SystemParameters.PrimaryScreenHeight < 1080)
            {
                //HD
                _resolution = 20;
                IsFHDEnabled = false;
                Is4KEnabled = false;
            }
            else if (1080 <= System.Windows.SystemParameters.PrimaryScreenHeight && System.Windows.SystemParameters.PrimaryScreenHeight < 2160)
            {
                //FHD
                _resolution = 30;
                Is4KEnabled = false;
            }
            else
            {
                //4K
                _resolution = 65;
            }
        }

        /// <summary>
        /// Sets the content of the tower selection buttons
        /// </summary>
        private void SetTowerButtonContent()
        {
            TowerButtons[0].Content = "25";
            TowerButtons[1].Content = "100";
            TowerButtons[2].Content = "25";
            TowerButtons[3].Content = "50";
            TowerButtons[4].Content = "X";
            TowerButtons[0].HP = 25;
            TowerButtons[1].HP = 100;
            TowerButtons[2].HP = 25;
            TowerButtons[3].HP = 50;
            TowerButtons[0].Background = "Content/mine.png";
            TowerButtons[1].Background = "Content/ultra_space_station_lvl1.gif";
            TowerButtons[2].Background = "Content/space_ship_lvl1.gif";
            TowerButtons[3].Background = "Content/space_station_lvl1.gif";
        }
        #endregion

        #region Game event handlers
        /// <summary>
        /// Refreshes table, visuals and tower selection buttons when the game advances
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_GameAdvanced(object sender, GameEventArgs e)
        {
            RefreshTable();
            UpdateVisuals();
            ControlTowerButtons();
        }

        /// <summary>
        /// Generates table and unlocks controls when the game is created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_GameCreated(object sender, GameEventArgs e)
        {
            GenerateTable();
            LockControls(false);
        }

        /// <summary>
        /// Locks the controls and updates the visuals when the game is over
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_GameOver(object sender, GameEventArgs e)
        {
            LockControls(true);
            UpdateVisuals();
        }

        /// <summary>
        /// Updates visuals and tower selection buttons when the time advances
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Model_GameTimeAdvanced(object sender, GameEventArgs e)
        {
            UpdateVisuals();
            ControlTowerButtons();
        }
        #endregion

        #region Event methods
        /// <summary>
        /// Raise the new game event
        /// </summary>
        private void OnNewGame()
        {
            if (NewGame != null)
                NewGame(this, EventArgs.Empty);
        }

        /// <summary>
        /// Raise the exit game event
        /// </summary>
        private void OnExitGame()
        {
            if (ExitGame != null)
                ExitGame(this, EventArgs.Empty);
        }

        /// <summary>
        /// Raise the pause game event
        /// </summary>
        private void OnPauseGame()
        {
            if (PauseGame != null)
                PauseGame(this, EventArgs.Empty);
        }

        /// <summary>
        /// Raise the pause music event
        /// </summary>
        private void OnPauseMusic()
        {
            if (PauseMusic != null)
                PauseMusic(this, EventArgs.Empty);
        }
        #endregion
    }
}
