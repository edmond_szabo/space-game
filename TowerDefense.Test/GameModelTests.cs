﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TowerDefense.Model;
using System;

namespace UnitTest
{
    [TestClass]
    public class GameModelTests
    {
        #region Fields
        private GameModel _model;
        private Boolean _gameOverTriggered;
        private Boolean _gameCreatedTriggered;
        private Boolean _gameAdvancedTriggered;
        private Boolean _gameTimeAdvancedTriggered;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor, sets triggered variables to false, and connects event handlers
        /// </summary>
        public GameModelTests()
        {
            _gameOverTriggered = false;
            _gameCreatedTriggered = false;
            _gameAdvancedTriggered = false;
            _gameTimeAdvancedTriggered = false;

            _model = new GameModel();
            _model.GameOver += new EventHandler<GameEventArgs>(Model_GameOver);
            _model.GameCreated += new EventHandler<GameEventArgs>(Model_GameCreated);
            _model.GameAdvanced += new EventHandler<GameEventArgs>(Model_GameAdvanced);
            _model.GameTimeAdvanced += new EventHandler<GameEventArgs>(Model_GameTimeAdvanced);
        }
        #endregion

        #region Event handlers
        /// <summary>
        /// Sets triggered variable to true
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Model_GameOver(Object sender, GameEventArgs e)
        {
            _gameOverTriggered = true;
        }

        /// <summary>
        /// Sets triggered variable to true
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Model_GameCreated(Object sender, GameEventArgs e)
        {
            _gameCreatedTriggered = true;
        }

        /// <summary>
        /// Sets triggered variable to true
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Model_GameAdvanced(Object sender, GameEventArgs e)
        {
            _gameAdvancedTriggered = true;
        }

        /// <summary>
        /// Sets triggered variable to true
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Model_GameTimeAdvanced(Object sender, GameEventArgs e)
        {
            _gameTimeAdvancedTriggered = true;
        }
        #endregion

        [TestMethod]
        public void TestInit()
        {
            // Check if initializing in NewGame really works
            Assert.IsNotNull(_model.Enemies);
            Assert.IsNotNull(_model.Towers);
            Assert.IsNotNull(_model.Mines);
            Assert.IsTrue(_model.Enemies.Count == 0);
            Assert.IsTrue(_model.Towers.Count == 0);
            Assert.IsTrue(_model.Mines.Count == 0);

            for (Int32 i = 0; i < 10; i++)
            {
                for (Int32 j = 0; j < 10; j++)
                {
                    Assert.IsTrue(_model.IsFreePos(i, j));
                }
            }
        }

        [TestMethod]
        public void TestNewGame()
        {
            _model.NewGame(GameDifficulty.Easy);

            Assert.IsTrue(_gameCreatedTriggered);

            Assert.IsTrue(_model.Enemies.Count == 0);
            Assert.IsTrue(_model.Towers.Count == 0);
            Assert.IsTrue(_model.Mines.Count == 0);
            Assert.IsTrue(_model.Gold == 800);

            for (int i = 0; i < 20; ++i)
            {
                _model.AdvanceTime();
            }

            Assert.IsTrue(_gameTimeAdvancedTriggered);

            Assert.AreEqual(_model.Time, 20);
            Assert.AreEqual(_model.WaveCount, 1);
            Assert.AreEqual(_model.WaveTimer, 0);

            _model.SpawnEnemies();
            Assert.IsTrue(_model.Enemies.Count == 1);

            _model.NewGame(GameDifficulty.Medium);
            Assert.IsTrue(_model.Enemies.Count == 0);
            Assert.IsTrue(_model.Gold == 700);

            _model.AdvanceTime();

            _model.NewGame(GameDifficulty.Hard);
            Assert.IsTrue(_model.Difficulty == GameDifficulty.Hard);
            Assert.IsTrue(_model.Gold == 600);
            Assert.AreEqual(_model.Time, 0);
        }

        [TestMethod]
        public void TestAddRemoveUnits()
        {
            _model.Enemies.Add(new Enemy(1, 1));
            _model.Enemies.Add(new Enemy(1, 2));
            _model.Enemies.Add(new Enemy(1, 3));
            _model.Enemies.Add(new Enemy(2, 1));

            Assert.IsFalse(_model.IsFreePos(1, 1));
            Assert.IsFalse(_model.IsFreePos(1, 2));
            Assert.IsFalse(_model.IsFreePos(1, 3));
            Assert.IsFalse(_model.IsFreePos(2, 1));
            Assert.IsTrue(_model.Enemies.Count == 4);

            _model.Mines.Add(new Mine(3, 1));
            _model.Mines.Add(new Mine(3, 2));
            _model.Mines.Add(new Mine(3, 3));

            Assert.IsFalse(_model.IsFreePos(3, 1));
            Assert.IsFalse(_model.IsFreePos(3, 2));
            Assert.IsFalse(_model.IsFreePos(3, 3));
            Assert.IsTrue(_model.IsFreePos(4, 4));
            Assert.IsTrue(_model.IsFreePos(4, 3));
            Assert.IsTrue(_model.Mines.Count == 3);
            Assert.IsFalse(_model.Mines.Count == _model.Enemies.Count);

            _model.Towers.Add(new T1(4, 4));
            _model.Towers.Add(new T2(4, 5));
            _model.Towers.Add(new T3(4, 6));

            Assert.IsFalse(_model.IsFreePos(4, 4));
            Assert.IsFalse(_model.IsFreePos(4, 5));
            Assert.IsFalse(_model.IsFreePos(4, 6));
            Assert.IsTrue(_model.Towers.Count == _model.Mines.Count);

            _model.RemoveTower(4, 4);
            _model.RemoveTower(4, 5);
            _model.RemoveTower(8, 8); // impossible
            _model.RemoveTower(3, 1);

            Assert.IsTrue(_model.IsFreePos(4, 4));
            Assert.IsTrue(_model.IsFreePos(4, 5));
            Assert.IsTrue(_model.IsFreePos(8, 8));
            Assert.IsTrue(_model.IsFreePos(3, 1));
            Assert.IsTrue(_model.Towers.Count == 1);
            Assert.IsTrue(_model.Mines.Count == 2);
            Assert.IsTrue(_model.Towers[0].X == 4 && _model.Towers[0].Y == 6);
            Assert.IsFalse(_model.IsFreePos(1, 2));
            Assert.IsFalse(_model.IsFreePos(1, 1));

            _model.RemoveUnit(1, 2);

            Assert.IsTrue(_model.Mines.Count == 2);
            Assert.IsTrue(_model.Enemies.Count == 3);

            _model.RemoveUnit(1, 1);

            Assert.IsTrue(_model.IsFreePos(1, 2));
            Assert.IsTrue(_model.IsFreePos(1, 1));
            Assert.IsTrue(_model.Enemies.Count == 2);
            Assert.IsTrue(_model.Mines.Count == 2);

            _model.RemoveUnit(1, 1); // removing non existent

            Assert.IsTrue(_model.IsFreePos(1, 1));
            Assert.IsTrue(_model.Enemies.Count == 2);

            Assert.IsTrue(_model.IsFreePos(3, 1));
            Assert.IsFalse(_model.IsFreePos(3, 3));
            _model.SpawnTower<Mine>(3, 1, (i, j) => new Mine(i, j)); // non existent
            _model.SpawnTower<Mine>(3, 3, (i, j) => new Mine(i, j)); // occupied

            Assert.IsFalse(_model.IsFreePos(3, 1));
            Assert.IsFalse(_model.IsFreePos(3, 3));
            Assert.IsTrue(_model.Mines.Count == 3);
            Assert.IsTrue(_model.IsFreePos(4, 4));
            Assert.IsTrue(_model.IsFreePos(4, 5));

            _model.SpawnTower<T2>(4, 4, (i, j) => new T2(i, j));
            _model.SpawnTower<T2>(4, 5, (i, j) => new T2(i, j));

            Assert.IsFalse(_model.IsFreePos(4, 4));
            Assert.IsFalse(_model.IsFreePos(4, 5));
            Assert.IsTrue(_model.Towers.Count == 3);

            _model.RemoveTower(200, 200); // not possible
            _model.RemoveTower(4, 5);

            Assert.IsTrue(_model.Towers.Count == 2);

            _model.SpawnTower(-1, -7, (i, j) => new T1(i, j));

            Assert.IsTrue(_model.Towers.Count == 2);
        }

        [TestMethod]
        public void TestUltimate()
        {
            _model.NewGame(GameDifficulty.Easy);

            for (Int32 i = 0; i < _model.TableSize.Item1; i++)
            {
                _model.Enemies.Add(new Enemy(i, 3));
            }

            for (Int32 i = 0; i < _model.TableSize.Item2; i++)
            {
                if(i!=3)
                _model.Enemies.Add(new Enemy(3, i));
            }

            Assert.AreEqual(_model.TableSize.Item1 + _model.TableSize.Item2 - 1, _model.Enemies.Count);

            _model.UltimateProgress = 100;
            _model.Ultimate(3, 3);

            Assert.AreEqual(0, _model.Enemies.Count);

            for (Int32 i = 0; i < _model.TableSize.Item1; i++)
            {
                _model.Enemies.Add(new Enemy(i, 3));
                _model.Enemies.Add(new Enemy(i, 2));
                _model.Enemies.Add(new Enemy(i, 4));
            }

            for (Int32 i = 0; i < _model.TableSize.Item2; i++)
            {
                if (i != 3 && i!=4 && i!= 2)
                {
                    _model.Enemies.Add(new Enemy(3, i));
                    _model.Enemies.Add(new Enemy(2, i));
                    _model.Enemies.Add(new Enemy(4, i));
                }                    
            }

            Assert.AreEqual((3*_model.TableSize.Item1) + (3*_model.TableSize.Item2) - 9, _model.Enemies.Count);

            _model.UltimateProgress = 100;
            _model.Ultimate(3, 3);
            
            Assert.AreEqual(0, _model.Enemies.Count);


            for (Int32 i = 0; i < _model.TableSize.Item1; i++)
            {
                _model.Enemies.Add(new Enemy(i, 0));
                _model.Enemies.Add(new Enemy(i, 1));
            }

            for (Int32 i = 0; i < _model.TableSize.Item2; i++)
            {
                if (i != 0 && i != 1)
                {
                    _model.Enemies.Add(new Enemy(0, i));
                    _model.Enemies.Add(new Enemy(1, i));
                }
            }
            Assert.AreEqual((2 * _model.TableSize.Item1) + (2 * _model.TableSize.Item2) - 4, _model.Enemies.Count);

            _model.UltimateProgress = 100;
            _model.Ultimate(1, 1);

            Assert.AreEqual(0, _model.Enemies.Count);

            // Ultimate progress is 0, so we cannot ultimate
            _model.Enemies.Add(new Enemy(5, 5));
            Assert.AreEqual(_model.Enemies.Count, 1);

            _model.Ultimate(5, 5);
            Assert.AreEqual(_model.Enemies.Count, 1);
        }

        [TestMethod]
        public void TestUpgrade()
        {
            _model.NewGame(GameDifficulty.Easy);

            _model.Towers.Add(new T1(1, 1));

            Assert.AreEqual(_model.Gold, 800);
            Assert.AreEqual(_model.Towers[0].Level, 1);

            _model.Upgrade(1, 1);

            // Gold should decrease by 100
            Assert.AreEqual(_model.Gold, 700);
            Assert.AreEqual(_model.Towers[0].Level, 2);

            _model.RemoveTower(1,1);

            // After removing, we should get half of the tower's price
            Assert.AreEqual(_model.Towers.Count, 0);
            Assert.AreEqual(_model.Gold, 750);

            // Let's try the other towers as well
            _model.Towers.Add(new T2(1, 2));
            _model.Towers.Add(new T3(1, 3));
            _model.Upgrade(1, 2);
            _model.Upgrade(1, 3);

            Assert.AreEqual(_model.Gold, 675);
            Assert.AreEqual(_model.Towers[0].Level, 2);
            Assert.AreEqual(_model.Towers[0].Level, 2);

            _model.Upgrade(1, 2);
            _model.Upgrade(1, 3);

            Assert.AreEqual(_model.Gold, 600);
            Assert.AreEqual(_model.Towers[0].Level, 3);
            Assert.AreEqual(_model.Towers[0].Level, 3);

            // Max levels, trying to upgrade once
            _model.Upgrade(1, 2);
            _model.Upgrade(1, 3);

            Assert.AreEqual(_model.Gold, 600);
            Assert.AreEqual(_model.Towers[0].Level, 3);
            Assert.AreEqual(_model.Towers[0].Level, 3);

            _model.RemoveTower(1, 2);
            _model.RemoveTower(1, 3);

            Assert.AreEqual(_model.Gold, 625+(25/2));
            Assert.AreEqual(_model.Towers.Count, 0);
        }

        [TestMethod]
        public void TestMoveEnemies()
        {
            _model.NewGame(GameDifficulty.Easy);

            Assert.AreEqual(_model.Enemies.Count, 0);
            
            // Advancing time so the waveTimer is < 0
            for(Int32 i = 0; i < 25; i++)
            {
                _model.AdvanceTime();
            }

            // Now we can spawn an enemy
            _model.SpawnEnemies();

            Assert.AreEqual(_model.Enemies.Count, 1);

            Int32 x = _model.Enemies[0].X;

            _model.Step();

            Assert.IsTrue(_gameAdvancedTriggered);

            Assert.AreEqual(x - 1, _model.Enemies[0].X);

            _model.Step();

            Assert.AreEqual(x - 2, _model.Enemies[0].X);

            // Nice! Enemies can step. Let's try with multiple enemies
            _model.SpawnEnemies();

            Assert.AreEqual(_model.Enemies.Count, 2);

            Int32 x2 = _model.Enemies[1].X;
            _model.Step();

            Assert.AreEqual(x - 3, _model.Enemies[0].X);
            Assert.AreEqual(x2 - 1, _model.Enemies[1].X);
            // Lol, it works
        }

        [TestMethod]
        public void TestTimeAdvancing()
        {
            _model.NewGame(GameDifficulty.Easy);
            Int32 gold = _model.Gold;

            Assert.AreEqual(_model.WaveTimer, 20);

            _model.Mines.Add(new Mine(1, 1));

            for(Int32 i=0; i < 20; i++)
            {
                _model.AdvanceTime();
            }

            Assert.AreEqual(_model.WaveTimer, 0);

            for (Int32 i = 0; i < 41; i++)
            {
                _model.AdvanceTime();
            }

            // Checking Wavetimer after resetting and gold should increase
            // by 82, because we only have 1 mine
            Assert.AreEqual(_model.WaveTimer, 19);
            Assert.AreEqual(_model.WaveCount, 2);
            Assert.AreEqual(_model.Gold, gold + 82);
        }

        [TestMethod]
        public void TestStep()
        {
            // T1, Mine and Enemy
            _model.NewGame(GameDifficulty.Easy);

            _model.Enemies.Add(new Enemy(5, 5));
            _model.SpawnTower(4, 5, (i, j) => new T1(i, j));

            _model.Step();

            Assert.AreEqual(_model.Enemies.Count, 0);
            Assert.AreEqual(_model.Towers.Count, 1);
            Assert.AreEqual(_model.Towers[0].HP, 2);

            _model.Enemies.Add(new Enemy(5, 5));

            _model.Step();

            Assert.AreEqual(_model.Enemies.Count, 0);
            Assert.AreEqual(_model.Towers.Count, 1);
            Assert.AreEqual(_model.Towers[0].HP, 1);

            _model.Enemies.Add(new Enemy(5, 5));

            _model.Step();

            Assert.AreEqual(_model.Enemies.Count, 1);
            Assert.AreEqual(_model.Towers.Count, 0);
            Assert.AreEqual(_model.Enemies[0].X, 4);

            _model.SpawnTower(3, 5, (i, j) => new Mine(i, j));

            Assert.AreEqual(_model.Mines.Count, 1);

            _model.Step();

            Assert.AreEqual(_model.Mines.Count, 0);

            // T2
            _model.NewGame(GameDifficulty.Easy);

            _model.Enemies.Add(new Enemy(15, 16));
            _model.SpawnTower(13, 16, (i, j) => new T2(i, j));

            Assert.AreEqual(_model.Enemies[0].MaxHP, 1);
            Assert.AreEqual(_model.Enemies.Count, 1);
            Assert.AreEqual(_model.Towers.Count, 1);
            Assert.AreEqual(_model.Towers[0].HP, 2);

            for (Int32 i = 0; i < 2; ++i)
            {
                _model.Step();
            }

            Assert.AreEqual(_model.Enemies.Count, 0);
            Assert.AreEqual(_model.Towers.Count, 1);
            Assert.AreEqual(_model.Towers[0].HP, 1);

            _model.Towers[0].UpgradeTower();
            _model.Enemies.Add(new Enemy(12, 16));
            _model.Enemies.Add(new Enemy(15, 16));

            for (Int32 i = 0; i < 2; ++i)
            {
                _model.Step();
            }

            Assert.AreEqual(_model.Towers.Count, 1);
            Assert.AreEqual(_model.Enemies.Count, 1);

            // T3
            _model.NewGame(GameDifficulty.Easy);

            _model.Enemies.Add(new Enemy(7, 8));
            _model.SpawnTower(7, 6, (i, j) => new T3(i, j));

            Assert.AreEqual(_model.Enemies.Count, 1);
            Assert.AreEqual(_model.Towers.Count, 1);

            _model.Step();

            Assert.AreEqual(_model.Enemies.Count, 0);
            Assert.AreEqual(_model.Towers.Count, 1);

            _model.Enemies.Add(new Enemy(7, 5));
            _model.Enemies.Add(new Enemy(7, 8));

            _model.Step();

            Assert.AreEqual(_model.Enemies.Count, 1);

            _model.Enemies.Add(new Enemy(7, 5));
            _model.Enemies.Add(new Enemy(7, 7));
            _model.Enemies.Add(new Enemy(7, 8));

            _model.Step();

            Assert.AreEqual(_model.Enemies.Count, 3);
        }

        [TestMethod]
        public void TestGameOver()
        {
            _model.NewGame(GameDifficulty.Easy);

            _model.Enemies.Add(new Enemy(0, 0));

            _model.Step();

            Assert.IsTrue(_gameOverTriggered);
        }
    }
}
